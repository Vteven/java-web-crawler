package com.jcasey;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class SpiderController {

	@Bean
	public Spider getSpider()
	{
		return new Spider(getHttpClient(), getHttpParser());
	}
	
	@Bean
	public HttpClient getHttpClient()
	{
		return new HttpClientImpl();
	}
	
	@Bean
	public HttpParser getHttpParser()
	{
		return new HttpParserImpl();
	}
	
}
