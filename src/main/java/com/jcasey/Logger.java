package com.jcasey;

import java.util.logging.Level;

public class Logger {

	/**
	 * Log a message
	 * @param message
	 * @param level
	 */
	public static void Log(String message, Level level)
	{
		if(level == Level.WARNING)
		{
			System.err.println(message);
		} else if(level == Level.INFO)
		{
			System.out.println(message);
		}
	}
	
}
