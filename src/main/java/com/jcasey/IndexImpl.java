package com.jcasey;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.document.Document;
import org.jsoup.Jsoup;

public class IndexImpl implements Index{


	
	public void beginIndex(Set<URI> search, JTable results, String term) throws IOException{
		
		DefaultTableModel tableModel = (DefaultTableModel)results.getModel();		

		if(search.size() > 0) tableModel.setRowCount(0);
            
		System.out.println("beginning index of : " + term);

		for (URI uri : search) {

				org.jsoup.nodes.Document soupDoc;
				try {
					soupDoc = Jsoup.connect(uri.toString()).timeout(120*1000).get();

	            String title = soupDoc.title();
	            String URL = uri.toString();
	            String body = soupDoc.body().text();

			    if (searchIndex(term, body, URL)) {

			    	tableModel.addRow(new String[]{title, URL, body});
			    }
			    
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    
				
		}
		
	}

	//code here references http://lucenetuts.blogspot.co.nz/2013/09/tutorial-working-with-lucene-and-eclipse.html
	
	private boolean searchIndex(String aterm, String abody, String aURL){
		
		boolean bool = false;

		try
		{
			//	Specify the analyzer for tokenizing text.
		    //	The same analyzer should be used for indexing and searching
			StandardAnalyzer analyzer = new StandardAnalyzer();
			
			//	Code to create the index
			Directory index = new RAMDirectory();
			
			IndexWriterConfig config = new IndexWriterConfig();
			
			IndexWriter w = new IndexWriter(index, config);
			
			addDoc(w, abody, aURL);
			
			w.close();
			
			//	Text to search
			String querystr = aterm;
			
			//	The \"title\" arg specifies the default field to use when no field is explicitly specified in the query
			Query q = new QueryParser("content", analyzer).parse(querystr);
			
			// Searching code
			int hitsPerPage = 10;
		    IndexReader reader = DirectoryReader.open(index);
		    IndexSearcher searcher = new IndexSearcher(reader);
//		    TopScoreDocCollector collector = TopScoreDocCollector.create(hitsPerPage, true);
		    TopScoreDocCollector collector = TopScoreDocCollector.create(hitsPerPage);
		    searcher.search(q, collector);
		    ScoreDoc[] hits = collector.topDocs().scoreDocs;
		    
		    //	Code to display the results of search
		    for(int i=0;i<hits.length;++i) 
		    {
		      int docId = hits[i].doc;
		      Document d = searcher.doc(docId);
		      System.out.println(d.get("url"));
		    }
		    
		    // reader can only be closed when there is no need to access the documents any more
		    reader.close();
		    if (hits.length == 1) {
		    	bool = true;

			}

		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		System.out.println("hit = " + bool);
		return bool;
		
	}
	
	private void addDoc(IndexWriter w, String content, String url) throws IOException 
	{
		  Document doc = new Document();
		  // A text field will be tokenized
		  doc.add(new TextField("content", content, Field.Store.YES));
		  // We use a string field for isbn because we don\'t want it tokenized
		  doc.add(new StringField("url", url, Field.Store.YES));
		  w.addDocument(doc);
	}
}
