package com.jcasey;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;

import org.springframework.stereotype.Component;

import jodd.jerry.Jerry;
import jodd.jerry.JerryFunction;

import static jodd.jerry.Jerry.jerry;


@Component
public class HttpParserImpl implements HttpParser {
	public final List<URI> parseLinks(final String content, final String host)
	{
		final List<URI> urls = new ArrayList <URI> ();
		// Create the Jerry document
		Jerry doc = jerry(content);
		// Loop over each a in the document
		doc.$("a").each(new JerryFunction() {
            public Boolean onNode(Jerry $this, int index) {
            	
            	// Create the URI and add it to the list
                try {
                	// Make the host always end with a /
                	String cleanedHost = host.endsWith ("/") ? host : host + "/";
                	// The current a's link
                	if($this.attr("href") == null) return false;;
            		String linkHref = new String($this.attr("href"));
                	/**
                	 * If the link is a local link (begins with a /) then the resulting uri is
                	 * host + href, otherwise the uri is just the href
                	 */
                	linkHref = linkHref.startsWith("/") ? cleanedHost + linkHref.replaceFirst("/", "") : linkHref;
                	if(validateURI(linkHref))
                		urls.add(new URI(linkHref));
				} catch (URISyntaxException e) {
					
					Logger.Log(e.getMessage(), Level.WARNING);
				}
                
                return true;
            }
        });
		
		return Collections.unmodifiableList(urls);
	}
	
	// Validate the uri
	private boolean validateURI(String uri)
	{
	    final URL url;
	    try {
	        url = new URL(uri);
	    } catch (Exception e1) {
	        return false;
	    }
	    return "http".equals(url.getProtocol());
	}
}
