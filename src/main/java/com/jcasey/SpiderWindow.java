package com.jcasey;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import net.miginfocom.swing.MigLayout;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;

import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.core.io.ClassPathResource;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class SpiderWindow {

	private JFrame frame;
	private JTextField txtUrl;
	private JTextField txtCrawlCount;
	private JTextField txtSearchTerm;
	private JTable tableResults;
	private Spider webCrawler;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SpiderWindow window = new SpiderWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SpiderWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 804, 492);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new MigLayout("", "[grow]", "[][grow][grow]"));
		
		txtUrl = new JTextField();
		txtUrl.setText("http://unitec.co.nz");
		panel.add(txtUrl, "flowx,cell 0 0,growx");
		txtUrl.setColumns(10);
		
		final JLabel lblFoundCount = new JLabel();
		lblFoundCount.setText("Stopped");
		
		JButton btnFindUrl = new JButton("Find Url");
		btnFindUrl.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int crawlCount = -1;
				
				try
				{
					crawlCount = Integer.parseInt(txtCrawlCount.getText());
				} catch(NumberFormatException ex)
				{
					JOptionPane.showMessageDialog(null, "You must enter a valid number starting from 0");
					return;
				}
				
				// Create our beans
				DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
				XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(beanFactory);
		        reader.loadBeanDefinitions(new ClassPathResource("spring.xml"));

		        // Start the crawler passing through the table so the results
		        // can be populated
				webCrawler = (Spider) beanFactory.getBean("Spider");
				webCrawler.startCrawl(tableResults, lblFoundCount, crawlCount);
			}
		});
		panel.add(btnFindUrl, "cell 0 0");
		
		JPanel panSettings = new JPanel();
		panSettings.setBorder(new TitledBorder(null, "Settings", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panSettings, "cell 0 0,growx,aligny top");
		panSettings.setLayout(new MigLayout("", "[][grow]", "[]"));
		
		JLabel lblUrlsToSearch = new JLabel("Max Urls");
		lblUrlsToSearch.setToolTipText("How many urls should we find?");
		panSettings.add(lblUrlsToSearch, "cell 0 0,alignx trailing");
		
		txtCrawlCount = new JTextField();
		txtCrawlCount.setText("2");
		panSettings.add(txtCrawlCount, "flowx,cell 0 0,alignx left");
		txtCrawlCount.setColumns(10);
		
		JLabel lblFound = new JLabel("Status:");
		panSettings.add(lblFound, "cell 1 0,alignx center,growy");
		
		panSettings.add(lblFoundCount, "cell 1 0,alignx right,growy");
		
		JPanel panResults = new JPanel();
		panel.add(panResults, "cell 0 1 1 2,grow");
		panResults.setLayout(new MigLayout("", "[691px,grow][65px]", "[23px][grow]"));
		
		txtSearchTerm = new JTextField();
		txtSearchTerm.setText("Please enter your search term here to look for it in the below URLs.");
		panResults.add(txtSearchTerm, "cell 0 0,growx,aligny center");
		txtSearchTerm.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		panResults.add(scrollPane, "cell 0 1 2 1,grow");
		
		tableResults = new JTable();
		scrollPane.setViewportView(tableResults);
		tableResults.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Title", "Url", "Body"
			}
		));
		
		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String searchterm = "";
				searchterm = txtSearchTerm.getText();
				System.out.println("searchterm = " + searchterm);
				
				// Create our beans
				DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
				XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(beanFactory);
		        reader.loadBeanDefinitions(new ClassPathResource("spring.xml"));

		        // Start the indexer passing through the table 
				Index index = (Index) beanFactory.getBean("Index");
				try {
					index.beginIndex(webCrawler.getCrawled(), tableResults, searchterm);
				} catch (IOException e1) {
					e1.printStackTrace();
				}

				
			}
		});
		
		panResults.add(btnSearch, "cell 1 0,alignx left,aligny top");

	}

}
