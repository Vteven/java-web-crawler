package com.jcasey;

import java.io.IOException;
import java.net.URI;
import java.util.Set;

import javax.swing.JTable;

public interface Index {
	public void beginIndex(Set<URI> search, JTable results, String term) throws IOException;
}
