package com.jcasey;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import org.springframework.stereotype.Component;

@Component
public class Spider {
	
	private HttpClient client;
	private HttpParser parser;
	
	private Set<URI> alreadyVisited = new HashSet<URI>();
	// Holds all the searched URIs
	private JTable results;
	private JLabel counter;
	
	public Spider(HttpClient client, HttpParser parser)
	{
		this.client = client;
		this.parser = parser;
	}
	
	public void startCrawl(JTable results, JLabel counter, int crawlCount)
	{
		this.results = results;
		this.counter = counter;
		
		try {
			crawl(new URI("http://www.unitec.ac.nz/"),crawlCount);
			
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}
	
	private boolean running = true;
	/**
	 * Crawls a specific uri for the links in the page
	 * 
	 * 
	 * Using breadth first search (BFS)
	 * 
	 * Also started in its own thread as to not block
	 * the main thread while the table is populated
	 * 
	 * Uses a queue system so several crawls can
	 * happen at the same time while populating the
	 * table
	 * 
	 * @param uri
	 * @param urlCount - The count of urls to search
	 */
	public void crawl(final URI uri, final int urlCount)
	{	
		new Thread()
		{
			public void run()
			{
				// Update the current thread, we only want
				// to be able to run the crawl one at a time
				int counted = 0;
				// Create the queue
				Queue<URI> queued = new LinkedList<URI>();
				// Add the base uri
				queued.add(uri);

				DefaultTableModel tableModel = (DefaultTableModel)results.getModel();
				tableModel.setRowCount(0);
				
				Outer:
				while(!queued.isEmpty() && running)
				{
					// Retrieved the head element and remove it
					URI popped = queued.poll();
					if(popped == null) break;
					// Holds the client response
					StringBuilder response = new StringBuilder();
					client.get(popped, response);
					// Get the links for the popped URI
					Iterator<URI> poppedSiblings = parser.parseLinks(response.toString(), popped.toString()).iterator();
					while(poppedSiblings.hasNext() && counted <= urlCount && running)
					{
						URI next = poppedSiblings.next();
						// All the next link to the already visited links
						// if it doesn't exist
						if(!alreadyVisited.contains(next))
						{
							tableModel.addRow(new String[]{next.toString()});
							alreadyVisited.add(next);
							// Queue the link to be crawled
							queued.add(next);
							counted++;
							
							if(counted >= urlCount)
							{
								break Outer;
							}
						}
					}

					counter.setText("Indexing...");
				}
				counter.setText("Complete");
			}
				
		}.start();
	}
	
	public Set<URI> getCrawled()
	{
		return alreadyVisited;
	}
}
