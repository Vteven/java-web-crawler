package com.jcasey;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.logging.Level;

import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Component;

@Component
public class HttpClientImpl implements HttpClient
{
	
	public boolean get(URI uri, StringBuilder htmlContent)
	{
		Logger.Log("Getting " + uri.toString(), Level.INFO);
		// Create the Apache http client
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpget = new HttpGet(uri);
		CloseableHttpResponse response = null;
		try 
		{
			
			response = httpclient.execute(httpget);		

			StatusLine statusLine = response.getStatusLine();
			HttpEntity entity = response.getEntity();
			
			// Create our status line
			if (statusLine.getStatusCode() >= 300) 
			{
				throw new HttpResponseException(
				statusLine.getStatusCode(),
				statusLine.getReasonPhrase());
			}
			
			if (entity == null) 
			{
				throw new ClientProtocolException("Response contains no content");
			}
			
			// Prepare the response to retrieve the data out of it
			ContentType contentType = ContentType.getOrDefault(entity);
			Charset charset = contentType.getCharset();
			if(charset == null) return true;
			Reader reader = new InputStreamReader(entity.getContent(), charset);
			
			// Response is been made now loop through the data
			while(reader.ready())
			{
				htmlContent.append((char)reader.read());
			}
		    
		} catch (ClientProtocolException e) 
		{
			e.printStackTrace();
		} catch (IOException e) 
		{
			e.printStackTrace();
		} finally
		{
			// Check if the response exists and close it
			if(response != null)
			{
				try 
				{
					response.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return false;
	}

}
