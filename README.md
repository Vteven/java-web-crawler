# README #

This is the README for a web crawler created in Java

### What is this repository for? ###

* A Java Web Crawler using the Spring, Apache and Jerry libraries
* Current version 1.1

### How do I get set up? ###

* Enter a URL to search the links for (Default http://unitec.ac.nz) and a depth to search for (default 10). Click Search
* Once this is done the program will start crawling
* Once the program starts filling the table you can enter some search terms to filter out some URLs and data. However it's best to wait until the crawling process finishes before searching

![77c2f791a5.png](https://bitbucket.org/repo/5qddknj/images/3058102205-77c2f791a5.png)